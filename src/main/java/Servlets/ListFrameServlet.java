package Servlets;

import JDBC.FrameDao;
import frame_pack.Frame;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@WebServlet(name = "ListFrameServlet",  urlPatterns= "/list")
public class ListFrameServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
//        PrintWriter writer = resp.getWriter();
//        writer.println("Method GET from AddServlet");

        List<Frame> allFrames = new ArrayList<>();
        String errorString = null;
        try
        {
            FrameDao dao = new FrameDao();
            allFrames = dao.getAllFrames();
        } catch (Exception e) {
            e.printStackTrace();
            errorString= e.getMessage();
        }
        req.setAttribute("errorString",errorString);
        req.setAttribute("frameList", allFrames);

        RequestDispatcher dispatcher = req.getServletContext()
                .getRequestDispatcher("/views/listFrame.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
