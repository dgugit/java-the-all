package JDBC;

import frame_pack.*;
import org.decimal4j.util.DoubleRounder;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FrameDao {


    public FrameDao() {
    }


    public Frame getFrameById(int frameID) {
        //Connection connection = ConnectionManager.getConnection();

        int ordernum;
        float height, width, perOne;
        String frName;
        Frame frame = null;

        String sqlQuery = "SELECT frame_id, fr_height, fr_width, per_fr_price,ordername FROM frame WHERE frame_id=?";
        try (PreparedStatement pStmt = ConnectionManager.getConnection().prepareStatement(sqlQuery)) {
            pStmt.setInt(1, frameID);
            ResultSet rez = pStmt.executeQuery();

            while (rez.next()) {
                ordernum = rez.getInt("frame_id");
                height = rez.getFloat("fr_height");
                width = rez.getFloat("fr_width");
                perOne = rez.getFloat("per_fr_price");
                frName = rez.getString("ordername");
                String[] args = {Float.toString(height), Float.toString(width), Float.toString(perOne)};
                frame = new Frame.Builder().buildFrame(FramesElementsDao.getElementsByFrameId(frameID, height, width), args);
                frame.setOrderNum(ordernum);
                frame.setOrderName(frName);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return frame;
    }

    public List<Frame> getFrameByName(String name)
    {
        String sqlQuary = "SELECT * FROM frame where position( ? in ordername)>0";
        int ordernum;
        float height, width, perOne;
        String frName;
        List<Frame> allFrames = new ArrayList<>();
        Frame frame = null;

        try(PreparedStatement pStmt = ConnectionManager.getConnection().prepareStatement(sqlQuary))
        {
            pStmt.setString(1,name);
            ResultSet rez = pStmt.executeQuery();


            while (rez.next()) {
                ordernum = rez.getInt("frame_id");
                height = rez.getFloat("fr_height");
                width = rez.getFloat("fr_width");
                perOne = rez.getFloat("per_fr_price");
                frName = rez.getString("ordername");
                String[] args = {Float.toString(height), Float.toString(width), Float.toString(perOne)};
                frame = new Frame.Builder().buildFrame(FramesElementsDao.getElementsByFrameId(ordernum, height, width), args);
                frame.setOrderNum(ordernum);
                frame.setOrderName(frName);
                allFrames.add(frame);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return allFrames;
    }

    public List<Frame> getAllFrames() {
        //Connection connection = ConnectionManager.getConnection();

        int ordernum;
        float height, width, perOne;
        String frName;
        List<Frame> allFrames = new ArrayList<>();
        Frame frame = null;

        String sqlQuery = "SELECT * FROM frame";
        try (PreparedStatement pStmt = ConnectionManager.getConnection().prepareStatement(sqlQuery)) {
            ResultSet rez = pStmt.executeQuery();

            while (rez.next()) {
                ordernum = rez.getInt("frame_id");
                height = rez.getFloat("fr_height");
                width = rez.getFloat("fr_width");
                perOne = rez.getFloat("per_fr_price");
                frName = rez.getString("ordername");
                String[] args = {Float.toString(height), Float.toString(width), Float.toString(perOne)};
                frame = new Frame.Builder().buildFrame(FramesElementsDao.getElementsByFrameId(ordernum, height, width), args);
                frame.setOrderNum(ordernum);
                frame.setOrderName(frName);
                allFrames.add(frame);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return allFrames;
    }


    public boolean addFrame(Frame frame) {
        String sqlInsertFrame = "INSERT INTO frame(fr_height, fr_width, per_fr_price, ordername) VALUES ( ?, ?, ? ,?);";
        String sqlInsertElements = "INSERT INTO frame_elem(frame_id, type, per_one_price) VALUES (?, ?, ?);";

        if (frame.getOrderNum() == 0) {
            try (/*Connection dbConnection = ConnectionManager.getConnection();*/
                 PreparedStatement prStmFrame = ConnectionManager.getConnection().prepareStatement(sqlInsertFrame, Statement.RETURN_GENERATED_KEYS);
                 PreparedStatement prStmElements = ConnectionManager.getConnection().prepareStatement(sqlInsertElements)) {

                prStmFrame.setDouble(1, DoubleRounder.round(frame.getHeightFrame(), 2));
                prStmFrame.setDouble(2, DoubleRounder.round(frame.getWidthFrame(), 2));
                prStmFrame.setDouble(3, DoubleRounder.round(frame.getPerOnePriceFrame(), 2));
                prStmFrame.setString(4,frame.getOrderName());
                prStmFrame.executeUpdate();

                ResultSet rs = prStmFrame.getGeneratedKeys();
                // rs.first();
                int id = 0;
                if (rs.next()) {
                    id = rs.getInt(1);
                }

                for (frameElement elem : frame.getElements()) {
                    if (elem.getClass().equals(Baget.class)) {
                        prStmElements.setInt(1, id);
                        prStmElements.setString(2, "Baget");
                        prStmElements.setDouble(3, DoubleRounder.round(elem.getPerOnePrice(), 2));
                    }
                    if (elem.getClass().equals(Fanera.class)) {
                        prStmElements.setInt(1, id);
                        prStmElements.setString(2, "Fanera");
                        prStmElements.setDouble(3, elem.getPerOnePrice());
                    }
                    if (elem.getClass().equals(Glass.class)) {
                        prStmElements.setInt(1, id);
                        prStmElements.setString(2, "Glass");
                        prStmElements.setDouble(3, elem.getPerOnePrice());
                    }
                    prStmElements.executeUpdate();
                }
                return true;

            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        } else return false;


    }

    public boolean updateFrame(Frame frame) {
        String sqlUpdateFrame = "UPDATE frame SET fr_height = ?, fr_width = ?, per_fr_price = ? , ordername = ? WHERE frame.frame_id = ? ;";
        String sqlUpdateElements = "UPDATE frame_elem SET type = ?, per_one_price = ? WHERE frame_elem.frame_id = ?;";

        if (frame.getOrderNum() != 0) {
            try (/*Connection dbConnection = ConnectionManager.getConnection();*/
                 PreparedStatement prStmFrame = ConnectionManager.getConnection().prepareStatement(sqlUpdateFrame);
                 PreparedStatement prStmElements = ConnectionManager.getConnection().prepareStatement(sqlUpdateElements)) {
                ConnectionManager.getConnection().setAutoCommit(false);

                prStmFrame.setDouble(1, DoubleRounder.round(frame.getHeightFrame(), 2));
                prStmFrame.setDouble(2, DoubleRounder.round(frame.getWidthFrame(), 2));
                prStmFrame.setDouble(3, DoubleRounder.round(frame.getPerOnePriceFrame(), 2));
                prStmFrame.setString(4,frame.getOrderName());
                prStmFrame.setInt(5, frame.getOrderNum());
                prStmFrame.executeUpdate();

                for (frameElement elem : frame.getElements()) {
                    if (elem.getClass().equals(Baget.class)) {
                        prStmElements.setString(1, "Baget");
                        prStmElements.setFloat(2, (float) elem.getPerOnePrice());
                        prStmElements.setInt(3, frame.getOrderNum());
                    }
                    if (elem.getClass().equals(Fanera.class)) {
                        prStmElements.setString(1, "Fanera");
                        prStmElements.setFloat(2, (float) elem.getPerOnePrice());
                        prStmElements.setInt(3, frame.getOrderNum());
                    }
                    if (elem.getClass().equals(Glass.class)) {
                        prStmElements.setString(1, "Glass");
                        prStmElements.setFloat(2, (float) elem.getPerOnePrice());
                        prStmElements.setInt(3, frame.getOrderNum());
                    }
                    prStmElements.addBatch();

                    ConnectionManager.getConnection().commit();
                    prStmElements.executeBatch();

                }
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }


        } else return false;

    }

    public boolean deleteFrame(Frame frame)
    {
        String sqlDeleteElem = "delete from frame_elem where frame_id = ? ;";
        String sqlDeleteFrame = "delete from frame where frame_id = ? ;";

        if(frame.getOrderNum() == 0 ) return false;
        {
            try (/*Connection dbConnection = ConnectionManager.getConnection();*/
                 PreparedStatement prStmElements = ConnectionManager.getConnection().prepareStatement(sqlDeleteElem);
                 PreparedStatement prStmFrame = ConnectionManager.getConnection().prepareStatement(sqlDeleteFrame)) {
                prStmElements.setInt(1, frame.getOrderNum());
                prStmElements.executeUpdate();
                prStmFrame.setInt(1, frame.getOrderNum());
                prStmFrame.executeUpdate();
                return true;

            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    public boolean deleteFrameByID(int frameId)
    {
        String sqlDeleteElem = "delete from frame_elem where frame_id = ? ;";
        String sqlDeleteFrame = "delete from frame where frame_id = ? ;";

        if(frameId == 0 ) return false;
        {
            try (/*Connection dbConnection = ConnectionManager.getConnection();*/
                 PreparedStatement prStmElements = ConnectionManager.getConnection().prepareStatement(sqlDeleteElem);
                 PreparedStatement prStmFrame = ConnectionManager.getConnection().prepareStatement(sqlDeleteFrame)) {
                prStmElements.setInt(1, frameId);
                prStmElements.executeUpdate();
                prStmFrame.setInt(1,frameId);
                prStmFrame.executeUpdate();

                return true;

            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
    }


}
