package Servlets;

import JDBC.FrameDao;
import frame_pack.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "AddFrameServlet",  urlPatterns= "/add")
public class AddFrameServlet extends HttpServlet
{

   public AddFrameServlet()
    {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        PrintWriter writer = resp.getWriter();
        writer.println("Method GET from AddServlet");
        req.getRequestDispatcher("/views/addFrame.jsp").forward(req, resp);



    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {

        String frName = req.getParameter("frameName");
        String frHeightStr = req.getParameter("frameHeight");
        String frWidthStr = req.getParameter("frameWidth");
        String frPriceStr = req.getParameter("framePrice");
        String bgPriceStr = req.getParameter("bagetPrice");
        String faPriceStr = req.getParameter("faneraPrice");
        String glPriceStr = req.getParameter("glassPrice");


        String errorString = null;
        float frHeight = 0;
        float frWidth = 0;
        float frPrice = 0;
        float bgPrice = 0;
        float faPrice = 0;
        float glPrice = 0;

        try
        {
            frHeight = Float.parseFloat(frHeightStr);
            frWidth  =  Float.parseFloat(frWidthStr);
            frPrice = Float.parseFloat(frPriceStr);
            bgPrice = Float.parseFloat(bgPriceStr);
            faPrice = Float.parseFloat(faPriceStr);
            glPrice = Float.parseFloat(glPriceStr);
        }
        catch (Exception e) {
            errorString = e.getMessage();
        }

            try
            {
                List<frameElement> elem = new ArrayList<>();
                if (bgPrice > 0)
                    elem.add(new Baget(frHeight, frWidth, bgPrice));

                if (faPrice > 0)
                    elem.add(new Fanera(frHeight, frWidth, faPrice));

                if (glPrice > 0)
                    elem.add(new Glass(frHeight, frWidth, glPrice));

                String[] args = {frHeightStr, frWidthStr, frPriceStr};
                Frame frame = new Frame.Builder().buildFrame(elem, args);
                frame.setOrderName(frName);
               System.out.println(frame.toString() + frame.getOrderName());

                try
                {
                    FrameDao dao = new FrameDao();
                    dao.addFrame(frame);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    errorString = errorString+ "\n"+ e.getMessage();
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
                errorString = errorString+ "\n"+ e.getMessage();
            }



        if (errorString != null)
        {
            RequestDispatcher dispatcher = req.getServletContext()
                    .getRequestDispatcher("/views/editFrame.jsp");
            dispatcher.forward(req, resp);
        }
        else
        {
            resp.sendRedirect(req.getContextPath() + "/list");
        }


    }
}
