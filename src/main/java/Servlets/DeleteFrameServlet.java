package Servlets;


import JDBC.FrameDao;
import frame_pack.Frame;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DeleteFrameServlet",  urlPatterns= "/delete")
public class DeleteFrameServlet extends HttpServlet
{
    public DeleteFrameServlet()
    {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        int frameId = Integer.parseInt((String)req.getParameter("delid"));

        String errorString = null;

        try
        {
            FrameDao dao = new FrameDao();
            dao.deleteFrameByID(frameId);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errorString= e.getMessage();
        }
        req.setAttribute("errorString",errorString);

        resp.sendRedirect(req.getContextPath() + "/list");
    }

}
