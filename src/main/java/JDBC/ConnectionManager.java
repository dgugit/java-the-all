package JDBC;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {
    private static Connection connection;

    private static Properties getProperties()
    {
//        FileInputStream fis;
      Properties property = new Properties();
//        try {
////            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
////            InputStream input = classLoader.getResourceAsStream("src/main/resources/DBconfiguration.properties");
//            //fis = new FileInputStream("DBconfiguration.properties");
//            //property.load(input);
//
//        } catch (IOException e) {
//            System.err.println("Error: DBconfig.properties has an error  ");
//        }
       return property;
    }

    private static void makeConnection() {
       // Properties properties = getProperties();

//        String jdbc_driver = properties.getProperty("jdbc.driver");
//        String url = properties.getProperty("jdbc.url");

        String jdbc_driver = "org.postgresql.Driver";
        String url = "jdbc:postgresql://localhost/javaframe";


        try {
            Class.forName(jdbc_driver);
            connection = DriverManager.getConnection(url, "postgres","02032004");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        if (connection == null)
            makeConnection();
        return connection;
    }


}
