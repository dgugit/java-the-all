package frame_pack;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class OrdersList
{
    List<Frame> OList = new ArrayList<>();

    public OrdersList()
    {
        OList = new ArrayList<Frame>();
    }


    public OrdersList(List<Frame> OList)
    {
        this.OList = OList;
    }

    public boolean addFrame(Frame frame)
    {
        return OList.add(frame);
    }

    public List<Frame> getFrames()
    {
        return OList;
    }

    /**
     * @return  those frames, that square is bigger than bigSize paramethre
     */
    public List<Frame> getBigFrames(float bigSize)
    {
        List<Frame> bigFrames = new LinkedList<Frame>();
        for (Frame fr : OList)
            if (fr.getSquare() >= bigSize)
                bigFrames.add(fr);
        return bigFrames;
    }

    public List<Frame> getBigFramesFilter(float bigSize)
    {
        return OList.stream().filter(Frame -> Frame.getSquare() >= bigSize).collect(Collectors.toList());
    }

    }
