<%--
  Created by IntelliJ IDEA.
  User: oleguk
  Date: 06.12.18
  Time: 18:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>FRAME VIEW </title>
    <style>
        body {
            font: 18px/20px 'Lucida Grande', Tahoma, Verdana, sans-serif;
            color: #404040;
            background: #d8e5f2;
            align-content: center;
        }
        #tablestyle {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 50%;
        }

        #tablestyle td, #tablestyle th {
            border: 3px solid #a2bedd;
            padding: 8px;
            text-align: center;
        }
        #tablestyle tr:nth-child(2n+1){background-color: #f2f2f2;}
        #tablestyle tr:nth-child(2n +1){background-color: #f2f2f2;}


        #tablestyle tr:hover {background-color: #ddd;}

        #tablestyle th {
            padding-top: 12px;
            padding-bottom: 12px;
            background-color: #59718a;
            text-align: center;
            color: white;
        }
    </style>
</head>
<body>
<br>
<br>
<h3 align="center">FRAME LIST</h3>
<p style="color: red;">${errorString}</p>
<table id="tablestyle"  align="center">
    <tr>
        <th>Order</th>
        <th>Height</th>
        <th>Width</th>
        <th>Price</th>
        <th>UPDATE</th>
        <th>DELETE</th>
    </tr>
    <c:forEach items="${frameList}" var="frame" >
        <tr>
            <td>${frame.orderName}</td>
            <td>${frame.heightFrame}</td>
            <td>${frame.widthFrame}</td>
            <td>${frame.calculatePrice()}</td>
            <td><a href="edit?updid=${frame.frameID}">UPDATE</a></td>
            <td><a href="delete?delid=${frame.frameID}">DELETE</a></td>

        </tr>
    </c:forEach>
</table>
<p align=center><a href="/"><img src="home.png" width= 40></a></p>
</body>
</html>