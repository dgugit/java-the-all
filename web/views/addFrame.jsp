<%--
  Created by IntelliJ IDEA.
  User: oleguk
  Date: 05.12.18
  Time: 19:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Frame</title>
    <style>
        body {
            font: 18px/20px 'Lucida Grande', Tahoma, Verdana, sans-serif;
            color: #404040;
            background: #d8e5f2;
            text-align: center;
        }
        button {
            display: inline-block;
            width: auto;
            height: auto;
            background-color: #3d6182;
            background-repeat: repeat-x;
            background-image: -khtml-gradient(linear, left top, left bottom, from(#8db7dd), to(#3d6182));
            background-image: -moz-linear-gradient(top, #8db7dd, #3d6182);
            background-image: -ms-linear-gradient(top, #8db7dd, #3d6182);
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #8db7dd), color-stop(100%, #3d6182));
            background-image: -webkit-linear-gradient(top, #8db7dd, #3d6182);
            background-image: -o-linear-gradient(top, #8db7dd, #3d6182);
            background-image: linear-gradient(top, #8db7dd, #3d6182);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#8db7dd', endColorstr='#3d6182', GradientType=0);
            text-shadow: 0 -1px 0 rgba(170, 184, 255, 0.4);
            border: 1px solid #7194dd;
            padding: 8px 17px 9px;
            color: white;
            cursor: pointer;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 0 rgba(162, 190, 221, 0.5), 0 1px 2px rgba(106, 120, 221, 0.51);
            -moz-box-shadow: inset 0 1px 0 rgba(162, 190, 221, 0.5), 0 1px 2px rgba(106, 120, 221, 0.51);
            box-shadow: inset 0 1px 0 rgba(162, 190, 221, 0.5), 0 1px 2px rgba(106, 120, 221, 0.51);
            -webkit-transition: 0.1s linear all;
            -moz-transition: 0.1s linear all;
            -ms-transition: 0.1s linear all;
            -o-transition: 0.1s linear all;
            transition: 0.1s linear all;
        }
        form {
            padding: 20px;
            font-family: Arial;
            font-size: 20px;
            display: inline-block;
            background-color: #d0d9f2;
            color: white;
            width: 400px;
            text-align: left;

        }
        label{
            padding-top: 6px;
            line-height: 18px;
            margin-right: 7px;
            float: left;
            color: #486bdd;
            width: 250px;
        }
        input,select,textarea {
            display: inline-block;
            width: 80px;
            height: 30px;
            padding: 4px;
            line-height: 18px;
            color: #486bdd;
            border: 1px solid #7194dd;
            background-color: rgba(193, 215, 221, 0.55);
            -webkit-transition: border linear 0.2s, box-shadow linear 0.2s;
            -moz-transition: border linear 0.2s, box-shadow linear 0.2s;
            -ms-transition: border linear 0.2s, box-shadow linear 0.2s;
            -o-transition: border linear 0.2s, box-shadow linear 0.2s;
            transition: border linear 0.2s, box-shadow linear 0.2s;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }
        div {
            clear: both;
            padding: 8px 0;
            overflow: hidden;
        }

    </style>
</head>
<body>

<br>
<br>

<h3 align="center">ADD ORDER </h3>
<p style="color: red;">${errorString}</p>
<form method="POST" action="${pageContext.request.contextPath}/add">
    <div>
        <label>Input order name: </label>
        <input type="text" name="frameName" value="aaaaa" align="right"/>
    </div>
    <div>
        <label>Frame height:</label>
        <input type="text" name="frameHeight" value="0.4" align="right"/>
    </div>
    <div>
        <label>Frame width:</label>
        <input type="text" name="frameWidth" value="0.3" align="right"/>
    </div>
    <div>
        <label>Frame perOnePrice:</label>
        <input type="text" name="framePrice" value="1" align="right"/>
    </div>
    <div>
        <label>Baget perOnePrice:</label>
        <input type="text" name="bagetPrice" value="100" align="right"/>
    </div>
    <div>
        <label>Fanera perOnePrice:</label>
        <input type="text" name="faneraPrice" value="200" align="right"/>
    </div>
    <div>
        <label>Glass perOnePrice:</label>
        <input type="text" name="glassPrice" value="300" align="right"/>
    </div>
    <br>
    <div align="center">
        <button type="submit"  value="Submit" style="align-content: center">ADD</button>
    </div>
    </table>
</form>

<p align=center><a href="/"><img src="home.png" width= 40></a></p>
</body>
</html>
