<%--
  Created by IntelliJ IDEA.
  User: oleguk
  Date: 06.12.18
  Time: 17:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>ORDER DETAILS</title>
    <meta charset="UTF-8">
    <style>
        body {
            font: 18px/20px 'Lucida Grande', Tahoma, Verdana, sans-serif;
            color: #404040;
            background: #d8e5f2;
            align-content: center;
            text-align: center;
        }
        button {
            display: inline-block;
            width: auto;
            height: auto;
            background-color: #3d6182;
            background-repeat: repeat-x;
            background-image: -khtml-gradient(linear, left top, left bottom, from(#8db7dd), to(#3d6182));
            background-image: -moz-linear-gradient(top, #8db7dd, #3d6182);
            background-image: -ms-linear-gradient(top, #8db7dd, #3d6182);
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #8db7dd), color-stop(100%, #3d6182));
            background-image: -webkit-linear-gradient(top, #8db7dd, #3d6182);
            background-image: -o-linear-gradient(top, #8db7dd, #3d6182);
            background-image: linear-gradient(top, #8db7dd, #3d6182);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#8db7dd', endColorstr='#3d6182', GradientType=0);
            text-shadow: 0 -1px 0 rgba(170, 184, 255, 0.4);
            border: 1px solid #7194dd;
            padding: 8px 17px 9px;
            color: white;
            cursor: pointer;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 0 rgba(162, 190, 221, 0.5), 0 1px 2px rgba(106, 120, 221, 0.51);
            -moz-box-shadow: inset 0 1px 0 rgba(162, 190, 221, 0.5), 0 1px 2px rgba(106, 120, 221, 0.51);
            box-shadow: inset 0 1px 0 rgba(162, 190, 221, 0.5), 0 1px 2px rgba(106, 120, 221, 0.51);
            -webkit-transition: 0.1s linear all;
            -moz-transition: 0.1s linear all;
            -ms-transition: 0.1s linear all;
            -o-transition: 0.1s linear all;
            transition: 0.1s linear all;
        }
        form {
            padding: 20px;
            font-family: Arial;
            font-size: 13px;
            display: inline-block;
            background-color: #d0d9f2;
            color: white;

        }
        label{
            color: #486bdd;
        }
        input,select,textarea {
            display: inline-block;
            width: 250px;
            height: 18px;
            padding: 4px;
            line-height: 18px;
            color: #486bdd;
            border: 1px solid #7194dd;
            background-color: rgba(193, 215, 221, 0.55);
            -webkit-transition: border linear 0.2s, box-shadow linear 0.2s;
            -moz-transition: border linear 0.2s, box-shadow linear 0.2s;
            -ms-transition: border linear 0.2s, box-shadow linear 0.2s;
            -o-transition: border linear 0.2s, box-shadow linear 0.2s;
            transition: border linear 0.2s, box-shadow linear 0.2s;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

    </style>
</head>
<body>
<br>
<br>
<h3 align="center">ORDER DETAILS</h3>
<p style="color: red;">${errorString}</p>
<form method="POST" action="${pageContext.request.contextPath}/frame">
    <div>
        <label>Order name: </label>
        <input type="text" name="frameName" value="aaaaa" align="center"/>
    </div>
    <br>
    <div align="center">
       <button type="submit"  value="Submit" style="align-content: center">SEARCH</button>
    </div>
        </table>
</form>
<p align=center><a href="/"><img src="home.png" width= 40></a></p>
</body>
</html>
