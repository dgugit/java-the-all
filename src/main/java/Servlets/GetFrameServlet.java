package Servlets;

import JDBC.FrameDao;
import frame_pack.Frame;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(name = "GetFrameServlet",  urlPatterns= "/frame")
public class GetFrameServlet extends HttpServlet
{
    public GetFrameServlet()
    {
        super();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        RequestDispatcher dispatcher = req.getServletContext()
                .getRequestDispatcher("/views/getFrame.jsp");
        dispatcher.forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String orderName  = (String) req.getParameter("frameName");
        List<Frame> selectedFrame = null;
        String errorString = null;

            try
            {
                FrameDao dao = new FrameDao();
                selectedFrame = dao.getFrameByName(orderName);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                errorString= e.getMessage();
            }
        req.setAttribute("errorString",errorString);
        req.setAttribute("frameList", selectedFrame);

        RequestDispatcher dispatcher = req.getServletContext()
                .getRequestDispatcher("/views/frameView.jsp");
        dispatcher.forward(req, resp);


    }
}
