import JDBC.FrameDao;
import frame_pack.*;

import javax.validation.Configuration;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;


public class Main
{
    public static void TestValidation() {
        List<frameElement> list1 = new ArrayList<>();
        Baget baget = new Baget((float) 0.3, (float) 0.4, 100);
        Fanera fanera = new Fanera((float) 0.3, (float) 0.4, 200);
        Glass glass = new Glass((float) 0.3, (float) 0.4, 300);

        list1.add(baget);
        list1.add(fanera);
        list1.add(glass);

        String[] frame1 = {"0.4", "0.3", "1"};
        Frame fr1 = new Frame.Builder().buildFrame(list1, frame1);

        list1.clear();
        String[] frame2 = {"0.1", "0.15", "1"};
        baget = new Baget((float) 0.1, (float) 0.15, 100);
        fanera = new Fanera((float) 0.1, (float) 0.15, 200);
        glass = new Glass((float) 0.1, (float) 0.15, 300);
        list1.add(baget);
        list1.add(fanera);
        list1.add(glass);
        Frame fr2 = new Frame.Builder().buildFrame(list1, frame2);

        list1.clear();
        String[] frame3 = {"0.5", "0.6", "1"};
        baget = new Baget((float) 0.5, (float) 0.6, 100);
        fanera = new Fanera((float) 0.5, (float) 0.6, 200);
        glass = new Glass((float) 0.5, (float) 0.6, 300);
        list1.add(baget);
        list1.add(fanera);
        list1.add(glass);
        Frame fr3 = new Frame.Builder().buildFrame(list1, frame3);

        System.out.println("Validation");

        Validator validator;
        //Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Configuration<?> config = Validation.byDefaultProvider().configure();
        ValidatorFactory factory = config.buildValidatorFactory();
        validator = factory.getValidator();
        factory.close();

        if (!Frame.validate(fr1, validator)) {
            System.out.println("Validation has not passed");
            return;
        }
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        if (!Frame.validate(fr2, validator)) {
            System.out.println("Validation has not passed");
            return;
        }
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        if (!Frame.validate(fr3, validator)) {
            System.out.println("Validation has not passed");
            return;
        }

    }

    public void TestStreames() {
        List<frameElement> list1 = new ArrayList<>();
        Baget baget = new Baget((float) 0.3, (float) 0.4, 100);
        Fanera fanera = new Fanera((float) 0.3, (float) 0.4, 200);
        Glass glass = new Glass((float) 0.3, (float) 0.4, 300);

        list1.add(baget);
        list1.add(fanera);
        list1.add(glass);

        String[] frame1 = {"0.4", "0.3", "1"};
        Frame fr1 = new Frame.Builder().buildFrame(list1, frame1);

        list1.clear();
        String[] frame2 = {"0.1", "0.15", "1"};
        baget = new Baget((float) 0.1, (float) 0.15, 100);
        fanera = new Fanera((float) 0.1, (float) 0.15, 200);
        glass = new Glass((float) 0.1, (float) 0.15, 300);
        list1.add(baget);
        list1.add(fanera);
        list1.add(glass);
        Frame fr2 = new Frame.Builder().buildFrame(list1, frame2);

        list1.clear();
        String[] frame3 = {"0.5", "0.6", "1"};
        baget = new Baget((float) 0.5, (float) 0.6, 100);
        fanera = new Fanera((float) 0.5, (float) 0.6, 200);
        glass = new Glass((float) 0.5, (float) 0.6, 300);
        list1.add(baget);
        list1.add(fanera);
        list1.add(glass);
        Frame fr3 = new Frame.Builder().buildFrame(list1, frame3);

        OrdersList MyOrders = new OrdersList();
        MyOrders.addFrame(fr1);
        MyOrders.addFrame(fr2);
        MyOrders.addFrame(fr3);
        System.out.println("\nStreams: ");
        MyOrders.getBigFrames((float) 0.12).forEach(item -> System.out.println(item.toString()));
        System.out.println();


    }

    public static void main(String[] args) {
        //Get Frame by ID
//            Frame fr = null;
//            try
//            {
//                FrameDao dao = new FrameDao();
//                fr = dao.getFrameById(5);
//                System.out.println("Readed frame:"+ "\n" + "Frame_id = "+ fr.getOrderNum()+ "\n"+ fr.toString());
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//            }

//         Get all frames
//        List<Frame> allFrames = new ArrayList<>();
//        try {
//            FrameDao dao = new FrameDao();
//            allFrames = dao.getAllFrames();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        for (Frame fr : allFrames
//        ) {
//
//            System.out.println("{ Frame_id = " + fr.getOrderName() + fr.getOrderNum() + "\n" + fr.toString() + "}");
//
//        }

        List<Frame> allFrames = new ArrayList<>();
        try {
            FrameDao dao = new FrameDao();
            allFrames = dao.getFrameByName("a7");
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (Frame fr : allFrames
        ) {

            System.out.println("{ Frame_id = " + fr.getOrderName() + fr.getOrderNum() + "\n" + fr.toString() + "}");

        }

//
//        List<frameElement> list = new ArrayList<>();
//        list.add(new Baget(30, 40, 300));
//        list.add(new Fanera(30, 40, 200));
//        list.add(new Glass(30, 40, 100));
//        String h = "0.3", w = "0.4", p = "1.0";
//
//        String[] argus = {h, w, p};
//        Frame fr = new Frame.Builder().buildFrame(list, argus);
//        System.out.println(fr);
//        fr.setOrderNum(9);
//
//
//        try
//        {
//            FrameDao dao = new FrameDao();
//            dao.deleteFrame(fr);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }



    }
}
