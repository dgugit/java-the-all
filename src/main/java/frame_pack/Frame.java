package frame_pack;

import javax.validation.*;
import javax.validation.constraints.DecimalMin;
import javax.xml.bind.annotation.*;
import java.lang.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@XmlRootElement
public class Frame
{
    private int frameID;
    private String orderName;

    @XmlElementWrapper( name = "elements" )
    @XmlElements( {
            @XmlElement( name="baget", type = Baget.class ),
            @XmlElement( name="fanera", type = Fanera.class),
            @XmlElement( name="glass", type = Glass.class )} )
    @ElementsValidation
    private List<frameElement> elements ;
    @XmlElement
    @DecimalMin(value = "0.05", message = "Minimal height is 0.05")
    private float heightFrame ;
    @XmlElement
    @DecimalMin(value = "0.05", message = "Minimal width is 0.05")
    private float widthFrame;
    @DecimalMin(value = "0.5", message = "Minimal price per One is 0.5")
    private float perOnePriceFrame;


    private final static float MIN_HEIGHT = (float)0.1;
    private final static float MIN_WIDTH = (float)0.1;
    private final static float MAX_HEIGHT = 2;
    private final static float MAX_WIDTH = 2;


    public Frame(){elements = new ArrayList<>();}
    public Frame(float ht, float wt)
    {
        this.heightFrame = ht * (float)0.01;
        this.widthFrame = wt * (float) 0.01;
        if (ht * wt > 0.12)
            perOnePriceFrame = 1;
        else
            perOnePriceFrame = (float) 0.5;
        elements = new ArrayList<>();

    }
    public Frame(List<frameElement> elements, float ht, float wt, float onePrice)
    {
        this.elements = elements;
        this.heightFrame = ht;
        this.widthFrame = wt;
        this.perOnePriceFrame = onePrice;
    }

    public float getHeightFrame() { return heightFrame; }
    public float getWidthFrame() {
        return widthFrame;
    }
    public float getPerOnePriceFrame() {
        return perOnePriceFrame;
    }

    public int getOrderNum() {
        return frameID;
    }

    public List<frameElement> getElements() {
        return elements;
    }

    public void setOrderNum(int orderNum) {
        this.frameID = orderNum;
    }

    public void setOrder(boolean boolBaget,boolean boolFanera,boolean boolGlass)
    {
        try {
            if (boolBaget  && !boolFanera && !boolGlass) this.OrderBaget();
            if (!boolBaget && boolFanera  && !boolGlass) this.OrderFanera();
            if (!boolBaget && !boolFanera && boolGlass ) this.OrderGlass();
            if (boolBaget && boolFanera && !boolGlass)
            {
                this.OrderBaget();
                this.OrderFanera();
            }
            if (boolBaget && !boolFanera && boolGlass)
            {
                this.OrderBaget();
                this.OrderGlass();
            }
            if (!boolBaget && boolFanera && boolGlass)
            {

                this.OrderFanera();
                this.OrderGlass();
            }
            if (boolBaget && boolFanera && boolGlass)
            {
                this.OrderBaget();
                this.OrderGlass();
                this.OrderFanera();

            }
        }
        catch (NullPointerException f){
            System.out.println("My error:");
            System.out.println(f.getMessage() + " " + f.getLocalizedMessage());
        }
    }

    private void OrderBaget()
    {
        Baget theBestBaget = new Baget(heightFrame,widthFrame, 100);
        elements.add(theBestBaget);
    }

    private void OrderGlass()
    {
        elements.add(new Glass(heightFrame,widthFrame,200));
    }

    private void OrderFanera()
    {
        elements.add(new Fanera(heightFrame,widthFrame,300));
    }

    public boolean equals(Object o)
    {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Frame frame = (Frame) o;
        return this.heightFrame == ((Frame) o).heightFrame && this.widthFrame == ((Frame) o).widthFrame && this.perOnePriceFrame == ((Frame) o).perOnePriceFrame;

    }

    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (frameID);
        result = prime * result + ((widthFrame == 0) ? 0 : Float.floatToIntBits(widthFrame));
        result = prime * result + ((heightFrame == 0) ? 0 : Float.floatToIntBits(heightFrame));
        result = prime * result + ((calculatePrice() == 0) ? 0 : Float.floatToIntBits(calculatePrice()));
        return result;

        //return Objects.hash(Order);
    }

    public float calculatePrice()
    {
        float Sum=0;
        for(frameElement f: elements)
        {
            Sum+= f.calculatePrice();
        }
        Sum += this.perOnePriceFrame;
        return Sum;
    }

    public float getSquare()
    {
        return heightFrame*widthFrame;
    }

    public String toString() {

        String result;
        result="elements";
        for( frameElement f: elements)
        {
         result += "\n" + f.getClass().toString();
         result+="\n" + f.getHeight();
         result+="\n" + f.getWidth();
         result+="\n" + f.getPerOnePrice();
        }
        result += "\n"+ "frame";
        result+="\n"+ this.getHeightFrame();
        result+="\n" + this.getWidthFrame();
        result+="\n" + this.getPerOnePriceFrame();
        return result;

//        ObjectMapper objMapper = new ObjectMapper();
//        objMapper.configure(com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT, true);
//        StringWriter stringDep = new StringWriter();
//
//        try {
//            objMapper.writeValue(stringDep, this);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return stringDep.toString();

    }

    public int getFrameID() {
        return frameID;
    }

    public void setFrameID(int frameID) {
        this.frameID = frameID;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }


    public static class Builder {

        private Frame frameToBuild;

        public Builder() {
            frameToBuild = new Frame();
        }

        public Frame build() throws IllegalArgumentException {
            Frame builtFrame = frameToBuild;
            frameToBuild = null;
            return builtFrame;
        }

        /**
         *
          * @param List of frame elements
         * @param args string fields of frame(Height,Width,PerOnePrice)
         * @return
         * @throws IllegalArgumentException
         */
        public Frame buildFrame(List<frameElement> List, String[] args)throws IllegalArgumentException {
            StringBuilder sb = new StringBuilder();
            try {
                this.setElements(List);
            } catch (IllegalArgumentException ex) {
                sb.append(ex.toString()).append("\n");
            }

            try {
                this.setHeight(Float.parseFloat(args[0]));
            } catch (IllegalArgumentException ex) {
                sb.append(ex.toString()).append("\n");
            }

            try {
                this.setWidth(Float.parseFloat(args[1]));
            } catch (IllegalArgumentException ex) {
                sb.append(ex.toString()).append("\n");
            }

            try {
                this.setPerOnePriceFrame(Float.parseFloat(args[2]));
            } catch (IllegalArgumentException ex) {
                sb.append(ex.toString()).append("\n");
            }

            if (sb.length() == 0)
                return this.build();
            throw new IllegalArgumentException(sb.toString());
        }

        public Builder setElements(List<frameElement> elem) {
            for(frameElement f: elem) {
                if(f.getPerOnePrice()<0)throw new IllegalArgumentException("Elements price for one item is incorrect!");
                if(f.getHeight()<0)throw new IllegalArgumentException("Elements height is incorrect!");
                if(f.getWidth()<0)throw new IllegalArgumentException("Elements width is incorrect!");
            }
            this.frameToBuild.elements = elem;
            return this;
        }

        public Builder setHeight(float ht) {
            if(ht >= MIN_HEIGHT && ht<=MAX_HEIGHT)
                this.frameToBuild.heightFrame= ht;
            else
                throw new IllegalArgumentException("Frame's height is incorrect!");
            return this;
        }

        public Builder setWidth(float wd) {
            if(wd >= MIN_WIDTH && wd<=MAX_WIDTH)
                this.frameToBuild.widthFrame= wd;
            else
                throw new IllegalArgumentException("Frame's width is incorrect!");
            return this;
        }

        public Builder setPerOnePriceFrame(float prOP) {
            if((prOP == 1 ||  prOP ==0.5) && prOP>0)
                this.frameToBuild.perOnePriceFrame= prOP;
            else
                throw new IllegalArgumentException("Frame's price for one item is incorrect!");
            return this;
        }

    }

    public static boolean validate(Object object, Validator validator) {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object);
        System.out.println(String.format("Number of errors: %d", constraintViolations.size()));

        for (ConstraintViolation<Object> cv : constraintViolations) {
            System.out.println(String.format("Error in validation! Property: [%s], value: [%s], message: [%s]", cv.getPropertyPath(), cv.getInvalidValue(), cv.getMessage()));
        }
        return constraintViolations.size() == 0;
    }


    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER,
            ElementType.ANNOTATION_TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(validatedBy = ElementsValidator.class)
    @Documented
    public @interface ElementsValidation {
        String message() default "Frame has to have any elements and they have to be different!";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    public class ElementsValidator implements ConstraintValidator<ElementsValidation, List<frameElement>> {
        public ElementsValidator() {
        }

        @Override
        public void initialize(ElementsValidation constraintAnnotation) {
        }

        @Override
        public boolean isValid(List<frameElement> value, ConstraintValidatorContext context) {
            if (value.isEmpty()) return false;
            else {
                for (int i = 0; i < value.size(); i++)
                    for (int j = 0; j < value.size() && j != i; j++)
                        if (value.get(i).getClass() == value.get(j).getClass())
                            return false;
            }
            return true;

        }
    }


}

