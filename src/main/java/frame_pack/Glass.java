package frame_pack;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "glass" )
public class Glass extends frameElement
{
    @JsonCreator
    public Glass(@JsonProperty("height")float height, @JsonProperty("width")float width, @JsonProperty("perOnePrice")float perOne)
    {
        super(height, width);
        this.perOnePrice = perOne;

    }

    public Glass(){}

}
